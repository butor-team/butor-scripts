#!/bin/bash 

#
#
# Link this file to start.sh and stop.sh in your mule folder
#
# Environment variables ::
#
# MULE_DIST : Location of Mule installation. Defaults to /opt/mule/mule-standalone-3.5.0/
#

export MULE_BASE=$(readlink -f $(dirname $0)/..)
export PIDDIR=${MULE_BASE}/temp/

if [[ -e ${MULE_BASE}/bin/setenv.sh ]]; then
	. ${MULE_BASE}/bin/setenv.sh
fi

if [[ -z "${MULE_DIST}" ]]; then
	MULE_DIST=/opt/mule/mule-standalone-3.5.0/
fi


export JAVA_HOME
export BUTOR_CONF_DIR
export BUTOR_HOSTNAME
export BUTOR_ENV
export APP_ID
export MULE_APP=${APP_ID}

cd ${MULE_BASE}


action=$(basename $0 .sh)
case "${action}" in
	console)
		${MULE_DIST}/bin/mule 
		;;
	start)
		${MULE_DIST}/bin/mule start
		;;
	 
	stop)
		${MULE_DIST}/bin/mule stop
		;;
	*)
		echo "Unknown action!"
		exit 2;
		;;
esac
