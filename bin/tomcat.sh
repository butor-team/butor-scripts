#!/bin/bash 

#
#
# Link this file to start.sh and stop.sh in your Tomcat folder
#
# e.g : 
# ln -sf  ln -sf /opt/butor/bin/tomcat.sh /opt/mytomcat/bin/stop.sh
# ln -sf  ln -sf /opt/butor/bin/tomcat.sh /opt/mytomcat/bin/start.sh
# 
# Environment variables ::
#
# TOMCAT_BASE : Location of Tomcat installation. Defaults to /opt/tomcat7/tomcat
#


export CATALINA_BASE=$(readlink -f $(dirname $0)/..)
export CATALINA_PID=${CATALINA_BASE}/temp/$(basename ${CATALINA_BASE}).pid

if [[ -z "${TOMCAT_BASE}" ]]; then
	TOMCAT_BASE=/opt/tomcat7/tomcat
fi

action=$(basename $0 .sh)
case "${action}" in
	start)
		${TOMCAT_BASE}/bin/startup.sh
		;;
	 
	stop)
		${TOMCAT_BASE}/bin/shutdown.sh
		;;
	*)
		echo "Unknown action!"
		exit 2;
		;;
esac
