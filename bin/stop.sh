#!/bin/bash

sln="$(readlink -f $0)"
scriptfolder="$(dirname $sln)"

. $scriptfolder/utils.sh
. $scriptfolder/set_app_env.sh

pid=$(getPid $APP_ID)
if [ -z $pid ]; then
  echo "No process running with app.id=$APP_ID"
  exit
fi
echo "Process running with pid=$pid"
STOP=${APP_BIN_DIR}/${STOP_PRG}

if [[ ! -x "${STOP}" ]]; then
  echo "$STOP is not executable or not found"
  exit 1
fi

if [[ "${USER}" == "${APP_USER}" ]]; then
	CMD="${STOP}&"
else
	CMD="sudo -b -E -u ${APP_USER} ${STOP}"
	#ask for sudo pwd before launching in backgound
	sudo -u $APP_USER /bin/true
fi

echo "Stoping with ${CMD} ..."
bash -c "${CMD}"

for i in {1..10}; do
  pid=$(getPid $APP_ID)
  if [ ! -z $pid ]; then
    echo -n "."
    sleep 1;
    continue
  fi
  echo "stoped"
  break
done
echo
pid=$(getPid $APP_ID)
if [ ! -z $pid ]; then
  echo "Killing that process!"
  sudo -u $USER kill -9 $pid
fi
pid=$(getPid $APP_ID)
if [ ! -z $pid ]; then
  echo "Didn't die! Giving up. Kill it yourself!"
else
  echo "Done"
fi

