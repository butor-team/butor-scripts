#!/bin/bash

[[ -z "${DEBUG}" ]] || echo "Setting BUTOR environment variable"

OPTS=$(getopt -o i:j:h:c:e: -l appid:,javahome:,hostname:,confdir:,envconfig: -- "$@")
if [ "$?" != 0 ]
then
    exit 1
fi

JAVA_HOME=/opt/jdk/java
if [[ -z  "${BUTOR_CONF_DIR}" ]]; then
	BUTOR_CONF_DIR=/opt/butor/conf/
fi

if [[ ! -d "${BUTOR_CONF_DIR}" ]]; then
	echo "Directory BUTOR_CONF_DIR : (${BUTOR_CONF_DIR} ) is missing"
	exit 1
fi

BUTOR_ENV_CONFIG=${BUTOR_CONF_DIR}/servers_env.conf
BUTOR_HOSTNAME="$(hostname -f)"

eval set -- "${OPTS}"

while true ; do
    case "$1" in
	--appid|-i)
		APP_ID=$2
		shift 2
		;;
	--java|-j)
		JAVA_HOME=$2
		shift 2
		;;
	--confdir|-c)
		BUTOR_CONF_DIR=$2
		shift 2
		;;
	--envconfig|-e)
		BUTOR_ENV_CONFIG=$2
		shift 2;
		;;
	--hostname|-h)
		BUTOR_HOSTNAME=$2
		shift 2;
		;;
	--) shift; break;;
    esac
done


if [ -z "${APP_ID}" ]; then
  echo "APP_ID not set! It should be"
  exit 1
fi

if [ -z "${BUTOR_HOSTNAME}" ]; then
  echo "BUTOR_HOSTNAME not set! It should be"
  exit 1
fi
if [[ -z "${BUTOR_ENV}" ]]; then
	BUTOR_ENV="$(grep ${BUTOR_HOSTNAME} ${BUTOR_ENV_CONFIG} | grep -v ^# |cut -d= -f2)"
	if [ -z "${BUTOR_ENV}" ]; then
	  echo "Cannot determine BUTOR_ENV. Host : ${BUTOR_HOSTNAME} not found. Check ${BUTOR_ENV_CONFIG}"
	  exit 1
	fi
fi

[[ -z "${DEBUG}" ]] || echo export JAVA_HOME=${JAVA_HOME}
[[ -z "${DEBUG}" ]] || echo export BUTOR_CONF_DIR=${BUTOR_CONF_DIR}
[[ -z "${DEBUG}" ]] || echo export BUTOR_HOSTNAME=${BUTOR_HOSTNAME}
[[ -z "${DEBUG}" ]] || echo export BUTOR_ENV=${BUTOR_ENV}

