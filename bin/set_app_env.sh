#!/bin/bash -x
[ -z "${DEBUG}" ] || echo "Loading set_app_env.sh ..."


parseArgs "$@";
checkAppId;


BINDIR=$(readlink -f $(dirname $0))

[ -z $DEBUG ] || echo "BINDIR=${BINDIR}"

function app_env_usage {
   echo "Missing/bad app config. Please see ${BUTOR_APP_CONF}"
}

readCfg


if [ -z "${START_PRG}" ]; then
  echo "missing START_PRG"
  app_env_usage
  exit -1
fi

if [ -z "${STOP_PRG}" ]; then
  echo "missing STOP_PRG"
  app_env_usage
  exit -1
fi

if [ -z "${APP_USER}" ]; then
  echo "missing APP_USER"
  app_env_usage
  exit -1
fi

if [ -z "${APP_ID}" ]; then
  echo "missing APP ID"
  app_env_usage
  exit -1
fi

