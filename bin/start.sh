#!/bin/bash 

scriptfolder="$(dirname $(readlink -f $0))"

. $scriptfolder/utils.sh
. $scriptfolder/set_app_env.sh

pid=$(getPid $APP_ID)
if [ ! -z $pid ]; then
  echo "Already up and running with pid $pid"
  exit
fi

START=${APP_BIN_DIR}/${START_PRG}

if [[ ! -x "${START}" ]]; then
  echo "$START is not executable or not found"
  exit 1
fi

if [[ "${USER}" == "${APP_USER}" ]]; then
	CMD="${START}"
else
	CMD="sudo -b -E -u ${APP_USER} ${START}"
fi

echo "Starting with ${CMD} ..."
bash -c ${CMD}

for i in {1..10}; do
  pid=$(getPid $APP_ID)
  if [ -z $pid ]; then
    echo -n "."
    sleep 1;
    continue
  fi
  echo "started with PID=$pid"
  break
done
if [ -z $pid ]; then
  echo "Giving up waiting for process to start!"
else
  echo "Done"
fi
