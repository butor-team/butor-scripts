#!/bin/bash


[ -z ${DEBUG} ] || echo "loaded utils.sh"
[ -z ${DEBUG} ] || echo "BUTOR_ENV = ${BUTOR_ENV}"



function getPid {
  APP_ID=$1
  if [ -z $APP_ID ]; then
    return
  fi
  PID=$(ps ax |grep "app.id=$APP_ID" | grep -v grep | grep -o "^ *[0-9]*")
  echo "${PID}"
}

function checkAppId {
	[ -z "${APP_ID}" ] && echo "You must provide appid using (--appid or -i)" && exit 2
}

function checkAction {
	[ -z "${ACTION}" ] && echo "You must provide action using (--action or -a)" && exit 2
}

function checkCfg {
	if [[ -z "${BUTOR_APP_CONFIG}" ]]; then
		echo "BUTOR_APP_CONFIG is not defined!"
		exit 2
	fi

	if [[ ! -e "${BUTOR_APP_CONFIG}" ]];then 
	 	echo "Unable to read config file ${BUTOR_APP_CONFIG} !";
		exit 2
	fi;
	BUTOR_APP_CONFIG=$(readlink -f ${BUTOR_APP_CONFIG})
}

function parseArgs {
	OPTS=$(getopt -o -a:-i:-c:h -l appid:,action:,config:,help -- "$@")
	if [ "$?" != 0 ]
	then
	    exit 1
	fi

	JAVA_HOME=/opt/jdk/java
	if [[ -z  "${BUTOR_CONF_DIR}" ]]; then
		BUTOR_CONF_DIR=/opt/butor/conf/
	fi

	if [[ ! -d "${BUTOR_CONF_DIR}" ]]; then
		echo "Directory BUTOR_CONF_DIR : (${BUTOR_CONF_DIR} ) is missing"
		exit 1
	fi

	BUTOR_APP_CONFIG=${BUTOR_CONF_DIR}/apps.conf
	BUTOR_HOSTNAME="$(hostname -f)"
	[[ -z "${ACTION}" ]] && ACTION=status;

	eval set -- "${OPTS}"

	while true ; do
	    case "$1" in
		-i|--appid)
			APP_ID=$2
			shift 2
			;;
		-h|--help)
			echo "Usage :"
			echo "--action / -a  : specify action (start, restart, stop, status)"
			echo "--config / -c  : specify a config file"
			echo "--appid / -i  : specify application id"
			exit 0;
			;;
		-a|--action)
			ACTION=$2
			shift 2
			;;
		-c|--config)
			BUTOR_APP_CONFIG=$2
			shift 2;
			checkCfg
			;;
		--) shift; break;;
		*)
			echo "Unknown arg : $1"
		 	exit 2;
			;;
	    esac
	done


	[ -z "${DEBUG}" ] || echo ACTION=${ACTION}
	[ -z "${DEBUG}" ] || echo APP_ID=${APP_ID}
	[ -z "${DEBUG}" ] || echo BUTOR_APP_CONFIG=${BUTOR_APP_CONFIG}
}

function readCfg {
	checkCfg
	[ -z "${DEBUG}" ] || echo "Loading app config : ${BUTOR_APP_CONFIG}";
	[ -z "${DEBUG}" ] || echo "Looking for APP_ID : ${APP_ID}";
	CFG="$(grep $APP_ID $BUTOR_APP_CONFIG | grep -v ^#)"
	[ -z ${DEBUG} ] || echo "CFG=${CFG}"
	if [[ -z "${CFG}" ]]; then
	  echo "Unable to find appid : $APP_ID in file config file : $BUTOR_APP_CONFIG";
	  exit 1
	fi

	IFS=,
	read APP_BIN_DIR APP_ID START_PRG STOP_PRG APP_USER <<< "${CFG}"
	if [ ! -z ${DEBUG} ]; then
	  echo "APP_BIN_DIR=${APP_BIN_DIR}"
	  echo "APP_ID=$APP_ID"
	  echo "START_PRG=$START_PRG"
	  echo "STOP_PRG=$STOP_PRG"
	  echo "APP_USER=$APP_USER"
	fi
}

